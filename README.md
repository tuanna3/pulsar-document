# Setup for pulsar cluster
-   [Requirements](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#requirements)
-   [Setup](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#setup)
	1. [Installing the Pulsar binary package](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#1-installing-the-pulsar-binary-package)
	2. [Deploying a Global-ZooKeeper cluster (optional)](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#2-deploying-a-global-zookeeper-cluster-optional)
	3. [Deploying a ZooKeeper cluster (optional)](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#3-deploying-a-zookeeper-cluster)
	4. [Initializing cluster metadata](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#4-initializing-cluster-metadata)
	5. [Deploying a Bookkeeper cluster](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#5-deploying-a-bookkeeper)
	6. [Deploying Pulsar brokers](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#6-deploying-pulsar-brokers)
	
---
Deploying a Pulsar cluster involves doing the following (in order):

-   Deploying a  [Global-ZooKeeper](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#2-deploying-a-global-zookeeper-cluster-optional)  cluster (optional)
-   Deploying a  [ZooKeeper](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#3-deploying-a-zookeeper-cluster)  cluster (optional)
-   Initializing  [cluster metadata](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#4-initializing-cluster-metadata)
-   Deploying a  [BookKeeper](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#5-deploying-a-bookkeeper)  cluster
-   Deploying one or more Pulsar  [brokers](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#6-deploying-pulsar-brokers)

## Requirements
- System
	- 1 Core
	- Ram: 2GB
	- Storage: 50GB 
	- Swap memory: 12GB ( you can setup step by step in [https://medium.com/@yenthanh/some-simple-things-for-tuning-your-ubuntu-server-3db99383eadb](https://medium.com/@yenthanh/some-simple-things-for-tuning-your-ubuntu-server-3db99383eadb) in part `Your server is out of memory? You may need some Swap memory` )
- Each machine in your cluster will need to have  [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)  or higher installed.
	You can install by ssh:
	``` bash
	$ sudo apt-get install openjdk-8-jdk
	export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
	export PATH=$JAVA_HOME/bin:$PATH
	```
	
## Setup

## 1. Installing the Pulsar binary package

### Install Pulsar apache
-   By clicking on the link directly below, which will automatically trigger a download:
	- [Pulsar 2.3.0 binary release](https://archive.apache.org/dist/pulsar/pulsar-2.3.0/apache-pulsar-2.3.0-bin.tar.gz)
-   From the Pulsar  [downloads page](https://pulsar.apache.org/download)
-   From the Pulsar  [releases page](https://github.com/apache/pulsar/releases/latest)  on  [GitHub](https://github.com/)
-   Using  [wget](https://www.gnu.org/software/wget):

```bash
$ wget https://archive.apache.org/dist/pulsar/pulsar-2.3.0/apache-pulsar-2.3.0-bin.tar.gz
```
Once you've downloaded the tarball, untar it and  `cd`  into the resulting directory:

```bash
$ tar xvzf apache-pulsar-2.3.0-bin.tar.gz
$ cd apache-pulsar-2.3.0
```

### Install Pulsar IO Connectors 

To get started using builtin connectors, you'll need to download the connectors tarball release on every broker node in one of the following ways:

-   by clicking the link below and downloading the release from an Apache mirror:
    
    -   [Pulsar IO Connectors 2.3.0 release](https://archive.apache.org/dist/pulsar/pulsar-2.3.0/apache-pulsar-io-connectors-2.3.0-bin.tar.gz)
-   from the Pulsar  [downloads page](https://pulsar.apache.org/download)
    
-   from the Pulsar  [releases page](https://github.com/apache/pulsar/releases/latest)
    
-   using  [wget](https://www.gnu.org/software/wget):
    
    ```shell
    $ wget https://archive.apache.org/dist/pulsar/pulsar-2.3.0/apache-pulsar-io-connectors-2.3.0-bin.tar.gz
    ```
    

Once the tarball is downloaded, in the pulsar directory, untar the io-connectors package and copy the connectors as  `connectors`  in the pulsar directory:

```bash
$ tar xvfz apache-pulsar-io-connectors-2.3.0-bin.tar.gz

// you will find a directory named `apache-pulsar-io-connectors-2.3.0` in the pulsar directory
// then copy the connectors

$ mv apache-pulsar-io-connectors-2.3.0/connectors connectors

$ ls connectors
pulsar-io-aerospike-2.3.0.nar
pulsar-io-cassandra-2.3.0.nar
pulsar-io-kafka-2.3.0.nar
pulsar-io-kinesis-2.3.0.nar
pulsar-io-rabbitmq-2.3.0.nar
pulsar-io-twitter-2.3.0.nar
...

```
### Download Certificate File and Secret Key
-   By clicking on the link directly below, which will automatically trigger a download:
	- [Certificate File](https://path-to-certificate)
	- [Secret File](https://path-to-secret-key)

-   Using  [wget](https://www.gnu.org/software/wget):

```bash
$ wget https://path-to-certificate
$ wget https://path-to-secret-key
```
Once you've downloaded the tarball, untar it and  `cd`  into the resulting directory:

```bash
$ tar xvzf ca-bin.tar.gz
$ mv ca apache-pulsar-2.3.0/ca
$ mv secret-key.key apache-pulsar-2.3.0/
```

### Result when completed to step
```bash
$ ls apache-pulsar-2.3.0
bin  ca  conf  connectors  data  examples  instances  lib  LICENSE  licenses  NOTICE  README secret-key.key
```
## 2. Deploying a Global-ZooKeeper cluster (optional)

### Configure
> If you already have an exsiting global-zookeeper cluster and would like to use it, you can skip this section.

To begin, add all Global-ZooKeeper servers to the configuration specified in  `conf/global-zookeeper.conf` (in the Pulsar directory you created  [above](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#1-installing-the-pulsar-binary-package)). Here's an example:

```properties
server.1=global-zk1.us.example.com:2185:2186
server.2=global-zk2.en.example.com:2185:2186
server.3=global-zk3.sgp.example.com:2185:2186
```

> You should make sure port use in global-zookeeper not open ( as the above example is `2185` and `2186`
> 
> If you have only one machine to deploy Pulsar, you just need to add one server entry in the configuration file.

On each host, you need to specify the ID of the node in each node's  `myid`  file, which is in each server's  `data/global-zookeeper`  folder by default (this can be changed via the  [`dataDir`](https://pulsar.apache.org/docs/en/reference-configuration#zookeeper-dataDir)  parameter).


On a Global-ZooKeeper server at  `global-zk1.us.example.com`, for example, you could set the  `myid`  value like this:

```bash
$ mkdir -p data/global-zookeeper
$ echo 1 > data/global-zookeeper/myid
```

On  `global-zk2.en.example.com`  the command would be  `echo 2 > data/global-zookeeper/myid`  and so on.

### Start global-zookeeper
Once each server has been added to the  `global-zookeeper.conf`  configuration and has the appropriate  `myid`entry, you can start Global-ZooKeeper on all hosts (in the background, using nohup) with the  [`pulsar-daemon`](https://pulsar.apache.org/docs/en/reference-cli-tools#pulsar-daemon)  CLI tool:

```bash
$ bin/pulsar-daemon start configuration-store
```
Or
```bash
$ bin/pulsar configuration-store
```
> If you are planning to deploy zookeeper with bookie on the same node, you need to start zookeeper by using different stats port.

Start zookeeper with  [`pulsar-daemon`](https://pulsar.apache.org/docs/en/reference-cli-tools#pulsar-daemon)  CLI tool like:

```bash
$ PULSAR_EXTRA_OPTS="-Dstats_server_port=8001" bin/pulsar-daemon start zookeeper
```
> You can change server port `8001` to other port.

## 3. Deploying a ZooKeeper cluster

### Configure
> If you already have an exsiting zookeeper cluster and would like to use it, you can skip this section.

> If you have only one machine to deploy Zookeeper, you can start zookeeper with [tutorial](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#start-zookeeper) .
> 
To begin, add all Global-ZooKeeper servers to the configuration specified in  `conf/zookeeper.conf` (in the Pulsar directory you created  [above](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#1-installing-the-pulsar-binary-package)). Here's an example:

```properties
server.1=zk1.us-west.example.com:2888:3888
server.2=zk2.us-west.example.com:2888:3888
server.3=zk3.us-west.example.com:2888:3888
```

> 
> You should make sure port use in global-zookeeper not open ( as the above example is `2888` and `3888`
> 
> If you have only one machine to deploy Pulsar, you just need to add one server entry in the configuration file.

On each host, you need to specify the ID of the node in each node's  `myid`  file, which is in each server's  `data/zookeeper`  folder by default (this can be changed via the  [`dataDir`](https://pulsar.apache.org/docs/en/reference-configuration#zookeeper-dataDir)  parameter).

On a ZooKeeper server at  `zk1.us-west.example.com`, for example, you could set the  `myid`  value like this:

```bash
$ mkdir -p data/zookeeper
$ echo 1 > data/zookeeper/myid
```

On  `zk2.us-west.example.com`  the command would be  `echo 2 > data/zookeeper/myid`  and so on.

### Start zookeeper
Once each server has been added to the  `zookeeper.conf`  configuration and has the appropriate  `myid`entry, you can start ZooKeeper on all hosts (in the background, using nohup) with the  [`pulsar-daemon`](https://pulsar.apache.org/docs/en/reference-cli-tools#pulsar-daemon)  CLI tool:

```bash
$ bin/pulsar-daemon start zookeeper
```
Or
```bash
$ bin/pulsar start zookeeper
```
> If you are planning to deploy zookeeper with bookie on the same node, you need to start zookeeper by using different stats port.

Start zookeeper with  [`pulsar-daemon`](https://pulsar.apache.org/docs/en/reference-cli-tools#pulsar-daemon)  CLI tool like:

```bash
$ PULSAR_EXTRA_OPTS="-Dstats_server_port=8001" bin/pulsar-daemon start zookeeper
```
> You can change server port `8001` to other port.

## 4. Initializing cluster metadata

Once you've deployed ZooKeeper for your cluster, there is some metadata that needs to be written to ZooKeeper for each cluster in your instance. It only needs to be written  **once**.

You can initialize this metadata using the  [`initialize-cluster-metadata`](https://pulsar.apache.org/docs/en/reference-cli-tools#pulsar-initialize-cluster-metadata)  command of the  [`pulsar`](https://pulsar.apache.org/docs/en/reference-cli-tools#pulsar)  CLI tool. This command can be run on any machine in your ZooKeeper cluster. Here's an example:

```bash
$ bin/pulsar initialize-cluster-metadata \
  --cluster ${cluster} \
  --zookeeper zk1.us-west.example.com:2181 \
  --configuration-store zk1.us-west.example.com:2181 \
  --web-service-url http://pulsar.us-west.example.com:8080 \
  --web-service-url-tls https://pulsar.us-west.example.com:8443 \
  --broker-service-url pulsar://pulsar.us-west.example.com:6650 \
  --broker-service-url-tls pulsar+ssl://pulsar.us-west.example.com:6651
```
Example:
```bash
$ bin/pulsar initialize-cluster-metadata \
  --cluster us-west \
  --zookeeper zk1.us-west.example.com:2181 \
  --configuration-store zk1.us-west.example.com:2181 \
  --web-service-url http://pulsar.us-west.example.com:8080 \
  --web-service-url-tls https://pulsar.us-west.example.com:8443 \
  --broker-service-url pulsar://pulsar.us-west.example.com:6650 \
  --broker-service-url-tls pulsar+ssl://pulsar.us-west.example.com:6651
```
As you can see from the example above, the following needs to be specified:


| Flag | Description|
|-------- | -------- |
|`--cluster` | A name for the cluster|
|`--zookeeper` | A "local" ZooKeeper connection string for the cluster. This connection string only needs to include  _one_  machine in the ZooKeeper cluster.|
|`--configuration-store` | The configuration store connection string for the entire instance. As with the  `--zookeeper`  flag, this connection string only needs to include  _one_  machine in the ZooKeeper cluster.|
|`--web-service-url` | The web service URL for the cluster, plus a port. This URL should be a standard DNS name. The default port is 8080 (we don't recommend using a different port).|
|`--web-service-url-tls`|If you're using  [TLS](https://pulsar.apache.org/docs/en/security-tls-transport), you'll also need to specify a TLS web service URL for the cluster. The default port is 8443 (we don't recommend using a different port).|
|`--broker-service-url`|A broker service URL enabling interaction with the brokers in the cluster. This URL should use the same DNS name as the web service URL but should use the  `pulsar`scheme instead. The default port is 6650 (we don't recommend using a different port).|
|`--broker-service-url-tls`|If you're using  [TLS](https://pulsar.apache.org/docs/en/security-tls-transport), you'll also need to specify a TLS web service URL for the cluster as well as a TLS broker service URL for the brokers in the cluster. The default port is 6651 (we don't recommend using a different port).|



## 5. Deploying a Bookkeeper

### Configuring Bookkeeper

Config deploy to bookkeeper server in `conf/bookkeeper.conf` (in the Pulsar directory you created  [above](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#1-installing-the-pulsar-binary-package)). You need to edit the config as follows :

```properties
# A list of one of more servers on which Zookeeper is running.
zkServers=zk1.us-west.example.com:2181,zk2.us-west.example.com:2181,zk3.us-west.example.com:2181

# Configure a specific hostname or IP address that the bookie should use to advertise itself to
# clients. If not set, bookie will advertised its own IP address or hostname, depending on the
# listeningInterface and useHostNameAsBookieID settings.
advertisedAddress= your-ip
```

### Start Bookkeeper

To start the bookie in the background, use the  [`pulsar-daemon`](https://pulsar.apache.org/docs/en/reference-cli-tools#pulsar-daemon)  CLI tool:

```bash
$ bin/pulsar-daemon start bookie
```


To start the bookie in the foreground:

```bash
$ bin/bookkeeper bookie
```
Or
```bash
$ bin/pulsar bookie
```
### Verify a bookie is working
You can verify that a bookie is working properly by running the  `bookiesanity`  command for the  [BookKeeper shell](https://pulsar.apache.org/docs/en/reference-cli-tools#shell)  on it:

```bash
$ bin/bookkeeper shell bookiesanity
```

This will create an ephemeral BookKeeper ledger on the local bookie, write a few entries, read them back, and finally delete the ledger.

After you have started all the bookies, you can use  `simpletest`  command for  [BookKeeper shell](https://pulsar.apache.org/docs/en/reference-cli-tools#shell)  on any bookie node, to verify all the bookies in the cluster are up running.

```bash
$ bin/bookkeeper shell simpletest --ensemble <num-bookies> --writeQuorum <num-bookies> --ackQuorum <num-bookies> --numEntries <num-entries>
```

This command will create a  `num-bookies`  sized ledger on the cluster, write a few entries, and finally delete the ledger.

## 6. Deploying Pulsar brokers


### Configuring Brokers

Config deploy to broker server in `conf/broker.conf` (in the Pulsar directory you created  [above](https://gitlab.com/tuanna3/pulsar-document/blob/master/README.md#1-installing-the-pulsar-binary-package)). You need to edit some config like this:

```properties
# Zookeeper quorum connection string
zookeeperServers=zk1.us-west.example.com:2181,zk2.us-west.example.com:2181,zk3.us-west.example.com:2181

# Configuration Store connection string
configurationStoreServers=global-zk1.us.example.com:2184

# Broker data port
brokerServicePort=6650

# Broker data port for TLS - By default TLS is disabled
brokerServicePortTls=6651

# Port to use to server HTTP request
webServicePort=8080

# Port to use to server HTTPS request - By default TLS is disabled
webServicePortTls=8443

# Number of threads to use for HTTP requests processing. Default is set to Runtime.getRuntime().availableProcessors()
numHttpServerThreads=8

# Hostname or IP address the service advertises to the outside world. If not set, the value of InetAddress.getLocalHost().getHostName() is used.
advertisedAddress=your-ip

# Name of the cluster to which this broker belongs to
clusterName=cluster-name

# Deprecated - Use webServicePortTls and brokerServicePortTls instead
tlsEnabled=true

# Path for the TLS certificate file
tlsCertificateFilePath=your-path/apache-pulsar-2.3.0/ca/broker.cert.pem

# Path for the TLS private key file
tlsKeyFilePath=your-path/apache-pulsar-2.3.0/ca/broker.key-pk8.pem

# Path for the trusted TLS certificate file.
# This cert is used to verify that any certs presented by connecting clients
# are signed by a certificate authority. If this verification
# fails, then the certs are untrusted and the connections are dropped.
tlsTrustCertsFilePath=your-path/apache-pulsar-2.3.0/ca/certs/ca.cert.pem

# Accept untrusted TLS certificate from client.
# If true, a client with a cert which cannot be verified with the
# 'tlsTrustCertsFilePath' cert will allowed to connect to the server,
# though the cert will not be used for client authentication.
tlsAllowInsecureConnection=false

# Specify the tls protocols the broker will use to negotiate during TLS handshake
# (a comma-separated list of protocol names).
# Examples:- [TLSv1.2, TLSv1.1, TLSv1]
tlsProtocols=TLSv1.2, TLSv1.1

# Specify the tls cipher the broker will use to negotiate during TLS Handshake
# (a comma-separated list of ciphers).
# Examples:- [TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256]
tlsCiphers=TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256

# Enable authentication
authenticationEnabled=true

# Autentication provider name list, which is comma separated list of class names
authenticationProviders=org.apache.pulsar.broker.authentication.AuthenticationProviderToken

# Enforce authorization
authorizationEnabled=true

# Authorization provider fully qualified class-name
authorizationProvider=org.apache.pulsar.broker.authorization.PulsarAuthorizationProvider

# Allow wildcard matching in authorization
# (wildcard matching only applicable if wildcard-char:
# * presents at first or last position eg: *.pulsar.service, pulsar.service.*)
authorizationAllowWildcardsMatching=false

# Role names that are treated as "super-user", meaning they will be able to do all admin
# operations and publish/consume from all topics
superUserRoles=admin

# Authentication settings of the broker itself. Used when the broker connects to other brokers,
# either in same or other clusters
brokerClientTlsEnabled=false
brokerClientAuthenticationPlugin=org.apache.pulsar.client.impl.auth.AuthenticationToken
brokerClientAuthenticationParameters=token:token-admin
brokerClientTrustCertsFilePath=

## Symmetric key
# Configure the secret key to be used to validate auth tokens
# The key can be specified like:
# tokenSecretKey=data:base64,xxxxxxxxx
# tokenSecretKey=file:///my/secret.key
tokenSecretKey=file:///my/secret.key
```


If you deploy Pulsar in a one-node cluster, you should update the replication settings in conf/broker.conf to 1

```properties
 # Number of bookies to use when creating a ledger
 managedLedgerDefaultEnsembleSize=1
 
 # Number of copies to store for each message
 managedLedgerDefaultWriteQuorum=1
 
 # Number of guaranteed copies (acks to wait before write is complete)
 managedLedgerDefaultAckQuorum=1
```


### Starting Brokers

You can then provide any other configuration changes that you'd like in the  [`conf/broker.conf`](https://pulsar.apache.org/docs/en/reference-configuration#broker)  file. Once you've decided on a configuration, you can start up the brokers for your Pulsar cluster. Like ZooKeeper and BookKeeper, brokers can be started either in the foreground or in the background, using nohup.

You can start a broker in the foreground using the  [`pulsar broker`](https://pulsar.apache.org/docs/en/reference-cli-tools#pulsar-broker)  command:

```bash
$ bin/pulsar broker
```

You can start a broker in the background using the  [`pulsar-daemon`](https://pulsar.apache.org/docs/en/reference-cli-tools#pulsar-daemon)  CLI tool:

```bash
$ bin/pulsar-daemon start broker
```

Once you've succesfully started up all the brokers you intend to use, your Pulsar cluster should be ready to go!


### Starting and Stop Bookie & Broker with screen available

#### Start
``` bash
$ screen  -x -X screen bash -c '/root/apache-pulsar-2.3.0/bin/pulsar-daemon start broker;/root/apache-pulsar-2.3.0/bin/pulsar-daemon start bookie; exec bash'
```

#### Stop
``` bash
$ screen  -x -X screen bash -c '/root/apache-pulsar-2.3.0/bin/pulsar-daemon stop broker;/root/apache-pulsar-2.3.0/bin/pulsar-daemon stop bookie; exec bash'
```
